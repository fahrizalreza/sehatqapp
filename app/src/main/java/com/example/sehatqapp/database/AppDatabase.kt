package com.example.sehatqapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.sehatqapp.model.HistoryProduct
import com.example.sehatqapp.model.Parent
import com.example.sehatqapp.utilities.AppConfig
import com.example.sehatqapp.utilities.ParentConverter

@Database(
//    entities = [HistoryProduct::class], version = AppConfig.DATABASE_VERSION
    entities = [HistoryProduct::class], version = AppConfig.DATABASE_VERSION
)
@TypeConverters(ParentConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun parametersDao(): ParametersDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java, AppConfig.DATABASE_NAME
                    )
                        .build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}