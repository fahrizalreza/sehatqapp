package com.example.sehatqapp.application

import android.app.Application
import com.example.sehatqapp.module.networkModule
import com.example.sehatqapp.module.repositoryModule
import com.example.sehatqapp.module.stateModule
import com.example.sehatqapp.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SehatQApp: Application() {

    // run koin to init dependency injection
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    // init koin modules
    private fun initKoin(){
        startKoin {
            androidContext(applicationContext)
            modules(listOf(networkModule, repositoryModule, stateModule, viewModelModule))
        }
    }
}