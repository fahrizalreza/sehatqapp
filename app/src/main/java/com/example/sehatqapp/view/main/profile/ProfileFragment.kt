package com.example.sehatqapp.view.main.profile

import android.util.Log
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatqapp.R
import com.example.sehatqapp.base.BaseFragment
import com.example.sehatqapp.databinding.ProfileFragmentBind
import com.example.sehatqapp.model.HistoryProduct
import com.example.sehatqapp.model.ProductItem
import com.example.sehatqapp.view.main.main.MainFragment
import org.koin.android.ext.android.inject


class ProfileFragment: BaseFragment<ProfileFragmentBind, ProfileFragmentState, ProfileFragmentViewModel>(
    R.layout.fragment_profile) {
    override val viewModel: ProfileFragmentViewModel by inject()

    override fun observeViewState(state: ProfileFragmentState) {
        state.currentState.observe(this, { currentState ->
            Log.d(
                LOG_PROFILE,
                "current state = ${state.currentState.value?.javaClass?.simpleName}"
            )
            when(currentState) {
                is ProfileState.OnSelectProduct -> openDetailProduct(currentState.product)
                is ProfileState.OnClickBack -> back()
            }
        })
    }

    override fun initializeView() {
        binding.rvProfileList.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        viewModel.start()
    }

    private fun openDetailProduct(item: HistoryProduct) {
        val product = ProductItem(item.loved, item.price, item.imageUrl, item.description, item.id, item.title)
        openDetailProduct(product)
    }

    private fun back() {
        NavHostFragment.findNavController(this).navigateUp()
//        NavHostFragment.findNavController(this).popBackStack()
    }

    // constant value
    companion object {
        const val LOG_PROFILE: String = "profileTrace"
    }
}