package com.example.sehatqapp.view.main.main

import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatqapp.R
import com.example.sehatqapp.base.BaseFragment
import com.example.sehatqapp.databinding.MainFragmentBind
import com.example.sehatqapp.view.login.LoginActivity
import org.koin.android.ext.android.inject

class MainFragment :
    BaseFragment<MainFragmentBind, MainFragmentState, MainFragmentViewModel>(R.layout.fragment_beranda) {

    override val viewModel: MainFragmentViewModel by inject()

    override fun initializeView() {
        binding.rvMainCategoryList.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding.rvMainProductSaleList.layoutManager =
            LinearLayoutManager(context?.applicationContext, RecyclerView.VERTICAL, false)
        viewModel.start()
    }

    override fun observeViewState(state: MainFragmentState) {
        state.currentState.observe(this, { currentState ->
            Log.d(
                LOG_MAIN_PRODUCT,
                "current state = ${state.currentState.value?.javaClass?.simpleName}"
            )
            when (currentState) {
                is MainFState.OpenSearch -> openSearchProduct()
                is MainFState.ShowScreenContent -> viewModel.loadVisibility.set(View.GONE)
                is MainFState.OnSelectProduct -> openDetailProduct(currentState.product)
            }
        })
    }

    // constant value
    companion object {
        const val LOG_MAIN_PRODUCT: String = "mainProductTrace"
    }
}