package com.example.sehatqapp.view.search

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.base.BaseViewModel
import com.example.sehatqapp.extension.RepositoryExtension.applySchedulers
import com.example.sehatqapp.model.ProductItem
import com.example.sehatqapp.repository.Repository
import io.reactivex.disposables.CompositeDisposable

class SearchViewModel(private val state: SearchActivityState, private val repository: Repository) :
    BaseViewModel() {
    override fun provideState(): BaseState = state
    val bindAdapter = ObservableField<SearchAdapter>()
    lateinit var adapter: SearchAdapter
    private val compositeDisposable = CompositeDisposable()

    val listProduct = mutableListOf<ProductItem>(
        ProductItem(
            0,
            "$340",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Nintendo-Switch-Console-Docked-wJoyConRB.jpg/430px-Nintendo-Switch-Console-Docked-wJoyConRB.jpg",
            "The Nintendo Switch was released on March 3, 2017 and is Nintendo's second entry in the eighth generation of home video game consoles. The system was code-named Nintendo NX prior to its official announcement. It is a hybrid device that can be used as a home console inserted to the Nintendo Switch Dock attached to a television, stood up on a table with the kickstand, or as a tablet-like portable console. It features two detachable wireless controllers called Joy-Con, that can be used individually or attached to a grip to provide a more traditional game pad form. Both Joy-Con are built with motion sensors and HD Rumble, Nintendo's haptic vibration feedback system for improved gameplay experiences. However, only the right Joy-Con has an NFC reader on its analog joystick for Amiibo and an IR sensor on the back. The Nintendo Switch Pro Controller is a traditional style controller much like the one of the Gamecube.",
            "6723",
            "Nitendo Switch"
        ),
        ProductItem(
            1,
            "$70",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/NES-Console-Set.jpg/430px-NES-Console-Set.jpg",
            "Released July 15, 1983, the Nintendo Entertainment System (NES) is an 8-bit video game console released by Nintendo in North America, South America, Europe, Asia, Oceania and Africa and was Nintendo's first home video game console released outside Japan. In Japan, it is known as the Family Computer (or Famicom, as it is commonly abbreviated). Selling 61.91 million units worldwide, the NES helped revitalize the video game industry following the video game crash of 1983 and set the standard for subsequent consoles in everything from game design to business practices. The NES was the first console for which the manufacturer openly courted third-party developers. Many of Nintendo's most iconic franchises, such as The Legend of Zelda and Metroid were started on the NES. Nintendo continued to repair Famicom consoles in Japan until October 31, 2007, attributing the decision to discontinue support to an increasing shortage of the necessary parts.[4][5][6]Nintendo released a software-emulation-based version of the Nintendo Entertainment System on November 10, 2016. Called the NES Classic Edition, it is a dedicated console that comes with a single controller and 30 preloaded games.[7]",
            "6724",
            "Nitendo Entertainment System "
        ),
        ProductItem(
            1,
            "$110",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Nintendo-ds-lite.svg/430px-Nintendo-ds-lite.svg.png",
            "The Nintendo DS (abbreviated NDS, DS, or the full name Nintendo Dual Screen, and iQue DS in China) is a handheld game console developed and manufactured by Nintendo, released in 2004. It is visibly distinguishable by its horizontal clamshell design, and the presence of two displays, the lower of which acts as a touchscreen. The system also has a built-in microphone and supports wireless IEEE 802.11 (Wi-Fi) standards, allowing players to interact with each other within short range (10–30 meters, depending on conditions) or over the Nintendo Wi-Fi Connection service via a standard Wi-Fi access point. According to Nintendo, the letters DS in the name stand for Developers' System and Double Screen, the former of which refers to the features of the handheld designed to encourage innovative gameplay ideas among developers.[43] The system was known as Project Nitro during development.",
            "6725",
            "Nitendo DS Lite "
        ),
        ProductItem(
            0,
            "$200",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Wii-console.jpg/430px-Wii-console.jpg",
            "The Nintendo DS (abbreviated NDS, DS, or the full name Nintendo Dual Screen, and iQue DS in China) is a handheld game console developed and manufactured by Nintendo, released in 2004. It is visibly distinguishable by its horizontal clamshell design, and the presence of two displays, the lower of which acts as a touchscreen. The system also has a built-in microphone and supports wireless IEEE 802.11 (Wi-Fi) standards, allowing players to interact with each other within short range (10–30 meters, depending on conditions) or over the Nintendo Wi-Fi Connection service via a standard Wi-Fi access point. According to Nintendo, the letters DS in the name stand for Developers' System and Double Screen, the former of which refers to the features of the handheld designed to encourage innovative gameplay ideas among developers.[43] The system was known as Project Nitro during development.",
            "6726",
            "Nitendo WII "
        )
    )

    fun start() {
        adapter = SearchAdapter(state)
        setAdapter(listProduct)
        setState(SearchState.FilterList(""))
    }

    // set list table on adapter
    fun setAdapter(items: List<ProductItem?>?) {
        adapter.setFilterList(items)
        adapter.submitList(items)
        adapter.notifyDataSetChanged()
        bindAdapter.set(adapter)
//        setState(SourceState.ShowScreenContent)
    }

    // filter table
    fun filterTable(char: CharSequence?) {
        adapter.filter.filter(char)
        adapter.notifyDataSetChanged()
        bindAdapter.set(adapter)
    }

    // back to previous screen
    fun onClickBack(view: View) {
        setState(SearchState.OnCLickBack)
    }

    // set current state in this activity
    fun setState(currentState: SearchState) {
        state.currentState.value = currentState
    }

    // constant value
    companion object {
        const val LOG_SEARCH: String = "searchTrace"
    }
}