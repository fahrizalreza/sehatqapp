package com.example.sehatqapp.view.main.main

import androidx.lifecycle.MutableLiveData
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.model.ProductItem
import com.example.sehatqapp.view.main.MainState

class MainFragmentState: BaseState {
    val currentState = MutableLiveData<MainFState>()
}

sealed class MainFState {
    object OpenSearch: MainFState()
    object ShowScreenContent: MainFState()
    data class OnSelectProduct(val product: ProductItem): MainFState()
}