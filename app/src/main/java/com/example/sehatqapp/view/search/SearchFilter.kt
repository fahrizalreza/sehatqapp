package com.example.sehatqapp.view.search

import android.util.Log
import android.widget.Filter
import com.example.sehatqapp.model.ProductItem
import java.util.*
import kotlin.collections.ArrayList

class SearchFilter(var adapter: SearchAdapter, val list: List<ProductItem?>?) : Filter() {

    // perform on user type character
    override fun performFiltering(constraint: CharSequence?): FilterResults {
        Log.d(LOG_SEARCH_FILTER, "process filter char = $constraint")
        val filterResult = FilterResults()
        val filteredSource = ArrayList<ProductItem>()

        if (!constraint.isNullOrBlank() && constraint.isNotEmpty()) {
            val charString = constraint.toString().lowercase(Locale.UK)

            for (item in list!!) {
                if (item?.title.toString().lowercase(Locale.UK).contains(charString)) {
                    filteredSource.add(item!!)
                }

            }
            filterResult.count = filteredSource.size
            filterResult.values = filteredSource
        } else {
            filterResult.count = filteredSource.size
            filterResult.values = filteredSource
//            filterResult.count = list?.size!!
//            filterResult.values = list
        }
        return filterResult
    }

    // publish filter result
    override fun publishResults(p0: CharSequence?, results: FilterResults?) {
        adapter.submitList(results?.values as ArrayList<ProductItem>)
        adapter.notifyDataSetChanged()
    }

    // constant value
    companion object {
        const val LOG_SEARCH_FILTER = "searchFilterTrace"
    }
}