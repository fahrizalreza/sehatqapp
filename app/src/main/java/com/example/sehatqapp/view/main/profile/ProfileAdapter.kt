package com.example.sehatqapp.view.main.profile

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.databinding.ItemProductBind
import com.example.sehatqapp.model.HistoryProduct

class ProfileAdapter(val state: BaseState) :
    ListAdapter<HistoryProduct, ProfileAdapter.ViewHolder>(Companion) {

    class ViewHolder(val binding: ItemProductBind) : RecyclerView.ViewHolder(binding.root)

    // attach layout to item view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemProductBind.inflate(layoutInflater)
        binding.adapter = this

        return ViewHolder(binding)
    }

    // attach item to adapter
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.item = item
        holder.binding.executePendingBindings()
    }

    // set table while user choose an item
    fun onClickItem(item: HistoryProduct) {
        Log.d(LOG_PROFILE_ADAPTER, "${item.title}")

        when (state) {
            is ProfileFragmentState -> state.currentState.value = ProfileState.OnSelectProduct(item)
        }
    }

    // constant value
    companion object : DiffUtil.ItemCallback<HistoryProduct>() {
        override fun areItemsTheSame(oldItem: HistoryProduct, newItem: HistoryProduct): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: HistoryProduct, newItem: HistoryProduct): Boolean =
            oldItem.id == newItem.id

        const val LOG_PROFILE_ADAPTER: String = "ProfileAdapterTrace"
    }
}