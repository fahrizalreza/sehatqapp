package com.example.sehatqapp.view.search

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sehatqapp.R
import com.example.sehatqapp.base.BaseActivity
import com.example.sehatqapp.databinding.SearchActivityBind
import com.example.sehatqapp.view.detailproduct.DetailProductActivity
import org.koin.android.ext.android.inject

class SearchActivity : BaseActivity<SearchActivityBind, SearchActivityState, SearchViewModel>(R.layout.activity_search) {
    override val viewModel: SearchViewModel by inject()

    override fun observeViewState(state: SearchActivityState) {
        Log.d(
            LOG_SEARCH,
            "current state = ${state.currentState.value?.javaClass?.simpleName}"
        )
        state.currentState.observe(this, { currentState ->
            when (currentState) {
                is SearchState.OnSelectProduct -> openDetailProduct(currentState.product)
                is SearchState.FilterList -> viewModel.filterTable(currentState.char)
                is SearchState.OnCLickBack -> closePage()
            }
        })
    }

    override fun initializeView() {
        super.initializeView()
        binding.rvSearchList.layoutManager = LinearLayoutManager(this)
        viewModel.start()
    }

    // constant value
    companion object {
        const val LOG_SEARCH: String = "searchTrace"
    }
}