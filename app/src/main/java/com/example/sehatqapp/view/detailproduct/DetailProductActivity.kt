package com.example.sehatqapp.view.detailproduct

import android.graphics.Color
import android.util.Log
import com.example.sehatqapp.R
import com.example.sehatqapp.base.BaseActivity
import com.example.sehatqapp.databinding.DetailProductActivityBind
import com.example.sehatqapp.view.main.main.MainFragment
import com.google.android.material.appbar.AppBarLayout
import org.koin.android.ext.android.inject

class DetailProductActivity :
    BaseActivity<DetailProductActivityBind, DetailProductActivityState, DetailProductViewModel>(R.layout.activity_detail_product) {
    override val viewModel: DetailProductViewModel by inject()
    private var isHide: Boolean = true

    override fun observeViewState(state: DetailProductActivityState) {
        state.currentState.observe(this, { currentState ->
            Log.d(
                LOG_DETAIL,
                "current state = ${state.currentState.value?.javaClass?.simpleName}"
            )
            when (currentState) {
                is DetailProductState.OnClickBack -> closePage()
                is DetailProductState.OnClickShared -> showToast(INFO_SHARED)
                is DetailProductState.OnClickFavourite -> infoFavourite(currentState.fav)
                is DetailProductState.SuccessInput -> showToast(INFO_SUCCESS)
            }
        })
    }

    override fun initializeView() {
        super.initializeView()
        setScrollAppBar()
        viewModel.start(this.intent)
    }

    private fun infoFavourite(item: Int) {
        if (item == 0) {
            showToast(INFO_FAV_REMOVED)
        } else {
            showToast(INFO_FAVOURITE)
        }
    }

    // perform when user scroll up or scroll down
    private fun setScrollAppBar() {

        // set title
        binding.ablDetailProductTitle.addOnOffsetChangedListener(object :
            AppBarLayout.OnOffsetChangedListener {
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }

                //When Scroll up, picture hide
                if (scrollRange + verticalOffset == 0) {
                    binding.tvDetailProductBack.text = viewModel.title.get()
                    binding.toolbarDetailProduct.setBackgroundColor(
                        resources.getColor(R.color.blue, null)
                    )
                    viewModel.arrow.set(1)
                    viewModel.shared.set(1)
                    isHide = true

                } else if (isHide) {

                    // when picture appear
                    binding.toolbarDetailProduct.setBackgroundColor(Color.TRANSPARENT)
                    binding.tvDetailProductBack.text = " "
                    //careful there should a space between double quote otherwise it wont work
                    viewModel.arrow.set(0)
                    viewModel.shared.set(0)
                    isHide = false
                }
            }
        })

    }

    // constant value
    companion object {
        const val INFO_SUCCESS = "Payment already submitted"
        const val INFO_SHARED = "Thanks for share item. The item already shared"
        const val INFO_FAVOURITE = "Favourite item"
        const val INFO_FAV_REMOVED = "Favourite removed"
        const val LOG_DETAIL: String = "detailTrace"
    }

}