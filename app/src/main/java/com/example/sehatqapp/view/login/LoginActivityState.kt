package com.example.sehatqapp.view.login

import androidx.lifecycle.MutableLiveData
import com.example.sehatqapp.base.BaseState

class LoginActivityState:BaseState {
    val currentState = MutableLiveData<LoginState>()
}

sealed class LoginState {
    object Initialize: LoginState()
    object OnClickLogin: LoginState()
    object OnClickPlayStore: LoginState()
    object OnClickAccount: LoginState()
}