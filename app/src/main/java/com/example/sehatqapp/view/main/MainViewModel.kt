package com.example.sehatqapp.view.main

import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.base.BaseViewModel

class MainViewModel(private val state: MainActivityState): BaseViewModel() {
    override fun provideState(): BaseState = state

    // set current state
    fun setState(currentState: MainState) {
        state.currentState.value = currentState
    }
}