package com.example.sehatqapp.view.main.main

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.base.BaseViewModel
import com.example.sehatqapp.extension.RepositoryExtension.applySchedulers
import com.example.sehatqapp.model.CategoryItem
import com.example.sehatqapp.model.ProductItem
import com.example.sehatqapp.repository.Repository
import com.example.sehatqapp.view.search.SearchViewModel
import io.reactivex.disposables.CompositeDisposable

class MainFragmentViewModel(
    private val state: MainFragmentState,
    private val repository: Repository
) : BaseViewModel() {
    override fun provideState(): BaseState = state
    val bindProductAdapter = ObservableField<MainProductSaleAdapter>()
    val bindAdapter = ObservableField<MainCategoryAdapter>()
    lateinit var productAdapter: MainProductSaleAdapter
    lateinit var adapter: MainCategoryAdapter
    private val compositeDisposable = CompositeDisposable()
    val loadVisibility = ObservableInt(View.GONE)

    // start view model
    fun start() {
        productAdapter = MainProductSaleAdapter(state)
        adapter = MainCategoryAdapter()

        loadData()
    }

    // load data from server
    private fun loadData() {
        loadVisibility.set(View.VISIBLE)

        val call = repository.remote.loadProduct()
        val disposable = call.applySchedulers().subscribe({ result ->
            Log.e(SearchViewModel.LOG_SEARCH, "connect to api master data = \n$result")
            val cat = result?.get(0)?.data?.category
            val items = result?.get(0)?.data?.product

            if (items.isNullOrEmpty()) {
//                setState(SearchState.ShowNoData)
            } else {
                setAdapter(cat)
                setProductAdapter(items)
            }

        }, { error ->
            loadVisibility.set(View.GONE)
            Log.e(
                SearchViewModel.LOG_SEARCH, "error connection: ${error.message} "
            )
            when (error) {
//                is HttpException -> setState(SearchState.ErrorSystem)
//                is IOException -> setState(SearchState.CheckConnection(PROCESS_LOAD_DATA))
//                else -> setState(SearchState.ErrorSystem)

            }
        })
        compositeDisposable.add(disposable)
    }

    // set list table on adapter
    fun setProductAdapter(items: List<ProductItem?>?) {
        productAdapter.submitList(items)
        productAdapter.notifyDataSetChanged()
        bindProductAdapter.set(productAdapter)
    }

    // set list table on adapter
    fun setAdapter(items: List<CategoryItem?>?) {
        adapter.submitList(items)
        adapter.notifyDataSetChanged()
        bindAdapter.set(adapter)
        setState(MainFState.ShowScreenContent)
    }

    fun onClickSearch(view: View) {
        setState(MainFState.OpenSearch)
    }

    // set current state in this activity
    fun setState(currentState: MainFState) {
        state.currentState.value = currentState
    }

}