package com.example.sehatqapp.view.search

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.databinding.ItemSearchBind
import com.example.sehatqapp.model.ProductItem

class SearchAdapter(val state: BaseState) :
    ListAdapter<ProductItem, SearchAdapter.ViewHolder>(Companion), Filterable {

    private var filteredList: List<ProductItem?>? = null

    class ViewHolder(val binding: ItemSearchBind) : RecyclerView.ViewHolder(binding.root)

    // attach layout to item view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemSearchBind.inflate(layoutInflater)
        binding.adapter = this

        return ViewHolder(binding)
    }

    // attach item to adapter
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.item = item
        holder.binding.executePendingBindings()
    }

    // set list so can be filtered
    fun setFilterList(list: List<ProductItem?>?) {
        filteredList = list
    }

    override fun getFilter(): Filter = SearchFilter(this, filteredList)

    // set table while user choose an item
    fun onClickItem(productItem: ProductItem) {
        Log.d(LOG_MAIN_ADAPTER, "${productItem.title}")

        when (state) {
            is SearchActivityState -> state.currentState.value = SearchState.OnSelectProduct(productItem)
        }
    }

    // constant value
    companion object : DiffUtil.ItemCallback<ProductItem>() {
        override fun areItemsTheSame(oldItem: ProductItem, newItem: ProductItem): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: ProductItem, newItem: ProductItem): Boolean =
            oldItem.id == newItem.id

        const val LOG_MAIN_ADAPTER: String = "MainProductAdapterTrace"
    }
}