package com.example.sehatqapp.view.detailproduct

import android.content.Intent
import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.viewModelScope
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.base.BaseViewModel
import com.example.sehatqapp.model.HistoryProduct
import com.example.sehatqapp.repository.Repository
import com.example.sehatqapp.utilities.Utilities
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailProductViewModel(
    private val state: DetailProductActivityState,
    private val repository: Repository
) : BaseViewModel() {

    override fun provideState(): BaseState = state
    val id = ObservableField("")
    val title = ObservableField("")
    val url = ObservableField("")
    val description = ObservableField("")
    val price = ObservableField("")
    val loved = ObservableInt(0)
    val arrow = ObservableInt(0)
    val shared = ObservableInt(0)

    // run on init
    fun start(intent: Intent) {
        id.set(intent.getStringExtra(KEY_PRODUCT_ID))
        title.set(intent.getStringExtra(KEY_PRODUCT_TITLE))
        url.set(intent.getStringExtra(KEY_PRODUCT_PICTURE_URL))
        description.set(intent.getStringExtra(KEY_PRODUCT_DESCRIPTION))
        price.set(intent.getStringExtra(KEY_PRODUCT_PRICE))
        loved.set(intent.getIntExtra(KEY_PRODUCT_LOVED, 0))
    }

    // insert into database
    fun saveItem(view: View) {
        val item = HistoryProduct(
            0,
            loved.get(),
            price.get(),
            url.get(),
            description.get(),
            id.get(),
            title.get(),
            Utilities.retrieveCurrentTime()
        )
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.local.insertHistoryProduct(item)
            }
            setState(DetailProductState.SuccessInput)
        }
    }

    // back to previous screen
    fun onClickBack(view: View) {
        setState(DetailProductState.OnClickBack)
    }

    // shared item info
    fun onClickShared(view: View) {
        setState(DetailProductState.OnClickShared)
    }

    fun onClickFavourite(view: View) {
        if (loved.get() == 0) loved.set(1) else loved.set(0)
        setState(DetailProductState.OnClickFavourite(loved.get()))
    }

    // set current state in this activity
    fun setState(currentState: DetailProductState) {
        state.currentState.value = currentState
    }

    // constant value
    companion object {
        const val KEY_PRODUCT_ID: String = "productId"
        const val KEY_PRODUCT_TITLE: String = "productTitle"
        const val KEY_PRODUCT_PICTURE_URL: String = "productURL"
        const val KEY_PRODUCT_DESCRIPTION: String = "productDescription"
        const val KEY_PRODUCT_PRICE: String = "productPrice"
        const val KEY_PRODUCT_LOVED: String = "productLoved"
    }
}