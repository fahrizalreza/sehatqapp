package com.example.sehatqapp.view.main.main

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.databinding.ItemProductSaleBind
import com.example.sehatqapp.model.ProductItem

class MainProductSaleAdapter(val state: BaseState) :
    ListAdapter<ProductItem, MainProductSaleAdapter.ViewHolder>(Companion) {

    class ViewHolder(val binding: ItemProductSaleBind) : RecyclerView.ViewHolder(binding.root)

    // attach layout to item view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemProductSaleBind.inflate(layoutInflater)
        binding.adapter = this

        return ViewHolder(binding)
    }

    // attach item to adapter
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.productItem = item
        holder.binding.executePendingBindings()
    }

    // set table while user choose an item
    fun onClickItem(productItem: ProductItem) {
        Log.d(LOG_MAIN_ADAPTER, "${productItem.title}")

        when (state) {
            is MainFragmentState -> state.currentState.value = MainFState.OnSelectProduct(productItem)
        }
    }

    // constant value
    companion object : DiffUtil.ItemCallback<ProductItem>() {
        override fun areItemsTheSame(oldItem: ProductItem, newItem: ProductItem): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: ProductItem, newItem: ProductItem): Boolean =
            oldItem.id == newItem.id

        const val LOG_MAIN_ADAPTER: String = "MainProductAdapterTrace"
    }
}