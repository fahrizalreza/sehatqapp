package com.example.sehatqapp.view.main

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.sehatqapp.R
import com.example.sehatqapp.base.BaseActivity
import com.example.sehatqapp.databinding.MainActivityBind
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.koin.android.ext.android.inject


class MainActivity : BaseActivity<MainActivityBind, MainActivityState, MainViewModel>(R.layout.activity_main) {

    override val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.setState(MainState.Initialize)
    }

    // monitor process here
    override fun observeViewState(state: MainActivityState) {
        state.currentState.observe(this, { currentState ->
            when(currentState) {
                is MainState.Initialize -> initialize()
                else -> clear()
            }

        })
    }

    // init view setup
    override fun initializeView() {
        super.initializeView()

        val navView: BottomNavigationView = binding.navView

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        val navController = navHostFragment!!.navController
        navView.setupWithNavController(navController)
    }

    // init additional
    private fun initialize() {}

    // perform when app idle
    private fun clear() {}
}