package com.example.sehatqapp.view.detailproduct

import androidx.lifecycle.MutableLiveData
import com.example.sehatqapp.base.BaseState

class DetailProductActivityState:BaseState {
    val currentState = MutableLiveData<DetailProductState>()
}

sealed class DetailProductState {
    object OnClickBack: DetailProductState()
    object OnClickShared: DetailProductState()
    data class OnClickFavourite(val fav: Int): DetailProductState()
    object SuccessInput: DetailProductState()
}