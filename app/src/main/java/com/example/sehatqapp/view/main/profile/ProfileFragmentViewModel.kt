package com.example.sehatqapp.view.main.profile

import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.base.BaseViewModel
import com.example.sehatqapp.model.CategoryItem
import com.example.sehatqapp.model.HistoryProduct
import com.example.sehatqapp.repository.Repository
import com.example.sehatqapp.view.main.main.MainFState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProfileFragmentViewModel(private val state: ProfileFragmentState, private val repository: Repository): BaseViewModel() {
    override fun provideState(): BaseState = state
    val bindAdapter = ObservableField<ProfileAdapter>()
    lateinit var adapter: ProfileAdapter

    fun start() {
        adapter = ProfileAdapter(state)
        loadHistoryProduct()
    }

    // read data from database
    fun loadHistoryProduct() {
        viewModelScope.launch {
            val list = withContext(Dispatchers.IO) { repository.local.loadHistoryPurchase()}
            setAdapter(list)
        }
    }

    // set list table on adapter
    private fun setAdapter(items: List<HistoryProduct?>?) {
        adapter.submitList(items)
        adapter.notifyDataSetChanged()
        bindAdapter.set(adapter)
//        setState(SourceState.ShowScreenContent)
    }

    fun onClickBack(view: View) {
        setState(ProfileState.OnClickBack)
    }

    // set current state in this activity
    fun setState(currentState: ProfileState) {
        state.currentState.value = currentState
    }


}