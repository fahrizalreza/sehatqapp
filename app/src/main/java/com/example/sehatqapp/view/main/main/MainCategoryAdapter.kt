package com.example.sehatqapp.view.main.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sehatqapp.databinding.ItemCategoryBind
import com.example.sehatqapp.model.CategoryItem

class MainCategoryAdapter() :
    ListAdapter<CategoryItem, MainCategoryAdapter.ViewHolder>(Companion) {

    class ViewHolder(val binding: ItemCategoryBind) : RecyclerView.ViewHolder(binding.root)

    // attach layout to item view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCategoryBind.inflate(layoutInflater)
        binding.adapter = this

        return ViewHolder(binding)
    }

    // attach item to adapter
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.item = item
        holder.binding.executePendingBindings()
    }

    // constant value
    companion object : DiffUtil.ItemCallback<CategoryItem>() {
        override fun areItemsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean =
            oldItem.id == newItem.id

        const val LOG_MAIN_ADAPTER: String = "MainAdapterTrace"
    }
}