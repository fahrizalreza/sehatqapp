package com.example.sehatqapp.view.login

import android.view.View
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.base.BaseViewModel

class LoginViewModel(private val state: LoginActivityState):BaseViewModel() {
    override fun provideState(): BaseState = state

    fun onClickLogin(view: View) {
        setState(LoginState.OnClickLogin)
    }

    fun onClickPlayStore(view: View) {
        setState(LoginState.OnClickPlayStore)
    }

    // set current state in this activity
    fun setState(currentState: LoginState) {
        state.currentState.value = currentState
    }
}