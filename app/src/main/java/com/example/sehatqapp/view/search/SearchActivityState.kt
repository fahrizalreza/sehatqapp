package com.example.sehatqapp.view.search

import androidx.lifecycle.MutableLiveData
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.model.ProductItem

class SearchActivityState : BaseState {
    val currentState = MutableLiveData<SearchState>()
}

sealed class SearchState {
    data class OnSelectProduct(val product: ProductItem): SearchState()
    data class FilterList(val char: CharSequence?): SearchState()
    object OnCLickBack : SearchState()
}