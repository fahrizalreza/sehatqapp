package com.example.sehatqapp.view.main.profile

import androidx.lifecycle.MutableLiveData
import com.example.sehatqapp.base.BaseState
import com.example.sehatqapp.model.HistoryProduct

class ProfileFragmentState: BaseState {
    val currentState = MutableLiveData<ProfileState>()
}

sealed class ProfileState {
    data class OnSelectProduct(val product: HistoryProduct): ProfileState()
    object OnClickBack: ProfileState()
}