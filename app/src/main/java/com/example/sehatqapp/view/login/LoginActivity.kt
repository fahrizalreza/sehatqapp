package com.example.sehatqapp.view.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.example.sehatqapp.R
import com.example.sehatqapp.base.BaseActivity
import com.example.sehatqapp.databinding.LoginActivityBind
import com.example.sehatqapp.view.main.MainActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import org.koin.android.ext.android.inject

class LoginActivity :
    BaseActivity<LoginActivityBind, LoginActivityState, LoginViewModel>(R.layout.activity_login) {
    private lateinit var callbackManager: CallbackManager
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var resultLauncher: ActivityResultLauncher<Intent>

    override val viewModel: LoginViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setAndroid()
        setFacebook()
        viewModel.setState(LoginState.Initialize)
    }

    // handle every event
    override fun observeViewState(state: LoginActivityState) {
        state.currentState.observe(this, { currentState ->
            Log.d(
                LOG_LOGIN,
                "current state = ${state.currentState.value?.javaClass?.simpleName}"
            )
            when (currentState) {
                is LoginState.Initialize -> initialize()
                is LoginState.OnClickLogin -> openMainMenu()
                is LoginState.OnClickPlayStore -> signInPlayStore()
                is LoginState.OnClickAccount -> openMainMenu()
                else -> clear()
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    // run on first time
    private fun initialize() {
    }

    // clear function
    private fun clear() {

    }

    // set android
    private fun setAndroid() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
//        val account = GoogleSignIn.getLastSignedInAccount(this)

        resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) {
                    val data = result.data
                    Log.d("success", "done $data")
                    val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                    task.addOnCompleteListener {
                        if (it.isSuccessful) {
                            val account: GoogleSignInAccount? =
                                it.getResult(ApiException::class.java)
                            val idToken = it.result?.idToken
                            val email = account?.email
                            val lastName = account?.familyName
                            val firstName = account?.givenName
                            val otherName = account?.displayName
                            val imageUrl = account?.photoUrl

                            Log.d("task", "Task successful \n$account")
                            Toast.makeText(this, "success", Toast.LENGTH_LONG).show()
                        } else {
                            Log.d("task", "Task not successful")
                        }
                    }
                } else if (result.resultCode == Activity.RESULT_CANCELED) {
                    Log.e("cancel", "cancel")
                }
                openMainMenu()
            }

        val btnPlayStore = findViewById<SignInButton>(R.id.btn_google)
        btnPlayStore.setOnClickListener { viewModel.setState(LoginState.OnClickPlayStore) }
    }

    private fun signInPlayStore() {
        val signInIntent = mGoogleSignInClient.signInIntent
        resultLauncher.launch(signInIntent)
    }

    // set facebook
    private fun setFacebook() {
        val btnFacebook = findViewById<LoginButton>(R.id.login_button)
//        btnFacebook.setPermissions(listOf("email", "public_profile"))
        callbackManager = CallbackManager.Factory.create()
        btnFacebook.registerCallback(callbackManager, object: FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.d(LOG_LOGIN,"onSuccess")
                viewModel.setState(LoginState.OnClickAccount)
            }

            override fun onCancel() {
                Log.d(LOG_LOGIN,"onCancel")
                viewModel.setState(LoginState.OnClickAccount)
            }

            override fun onError(error: FacebookException?) {
                Log.d(LOG_LOGIN,"onError = ${error?.localizedMessage}")
            }
        })
    }

    // open main menu
    private fun openMainMenu() {
        val intent = Intent(this, MainActivity::class.java)
        val tes = MainActivity::class.java.name
        Log.d(LOG_LOGIN, tes)
        startActivity(intent)
        finish()
    }

    // constant value
    companion object {
        const val LOG_LOGIN: String = "loginTrace"
    }
}