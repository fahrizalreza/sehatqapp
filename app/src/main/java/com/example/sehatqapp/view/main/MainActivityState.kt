package com.example.sehatqapp.view.main

import androidx.lifecycle.MutableLiveData
import com.example.sehatqapp.base.BaseState

class MainActivityState: BaseState {
    val currentState = MutableLiveData<MainState>()
}

sealed class MainState {
    object Initialize: MainState()
    data class OnSelectProduct(val product: String): MainState()
}