package com.example.sehatqapp.model

import com.google.gson.annotations.SerializedName

data class ProductData (

    @field:SerializedName("productPromo")
    val productPromo: List<ProductItem?>? = null,

    @field:SerializedName("category")
    val category: List<CategoryItem?>? = null
)