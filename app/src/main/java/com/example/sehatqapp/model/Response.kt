package com.example.sehatqapp.model

import com.google.gson.annotations.SerializedName

data class Dataz(
	@field:SerializedName("productPromo")
	val product: List<ProductItem?>? = null,

	@field:SerializedName("category")
	val category: List<CategoryItem?>? = null
)

data class ResponseItemz(
	@field:SerializedName("data")
	val data: Dataz? = null
)
