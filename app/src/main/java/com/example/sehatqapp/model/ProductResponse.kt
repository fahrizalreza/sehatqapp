package com.example.sehatqapp.model

import com.google.gson.annotations.SerializedName

data class ProductResponse (

    @field:SerializedName("data")
    val data: ProductData? = null
)