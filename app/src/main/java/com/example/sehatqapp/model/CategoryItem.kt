package com.example.sehatqapp.model

import com.google.gson.annotations.SerializedName

data class CategoryItem (

    @field:SerializedName("imageUrl")
    val imageUrl: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)