package com.example.sehatqapp.repository

import com.example.sehatqapp.database.ParametersDao
import com.example.sehatqapp.network.ApiService

class Repository(remote: ApiService, local: ParametersDao) {
    val remote = remote
    val local = local
}