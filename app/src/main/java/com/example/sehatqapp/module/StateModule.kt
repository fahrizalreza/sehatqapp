package com.example.sehatqapp.module

import com.example.sehatqapp.view.detailproduct.DetailProductActivityState
import com.example.sehatqapp.view.login.LoginActivityState
import com.example.sehatqapp.view.main.MainActivityState
import com.example.sehatqapp.view.main.main.MainFragmentState
import com.example.sehatqapp.view.main.profile.ProfileFragmentState
import com.example.sehatqapp.view.search.SearchActivityState
import org.koin.dsl.module

val stateModule = module {
    factory { LoginActivityState() }
    factory { MainActivityState() }
    factory { MainFragmentState() }
    factory { ProfileFragmentState() }
    factory { DetailProductActivityState() }
    factory { SearchActivityState() }
}