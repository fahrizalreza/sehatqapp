package com.example.sehatqapp.module

import com.example.sehatqapp.view.detailproduct.DetailProductViewModel
import com.example.sehatqapp.view.login.LoginViewModel
import com.example.sehatqapp.view.main.MainViewModel
import com.example.sehatqapp.view.main.main.MainFragmentViewModel
import com.example.sehatqapp.view.main.profile.ProfileFragmentViewModel
import com.example.sehatqapp.view.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { MainViewModel(get()) }
    viewModel { MainFragmentViewModel(get(), get()) }
    viewModel { ProfileFragmentViewModel(get(), get()) }
    viewModel { DetailProductViewModel(get(), get()) }
    viewModel { SearchViewModel(get(), get()) }
}