package com.example.sehatqapp.module

import android.content.Context
import com.example.sehatqapp.database.AppDatabase
import com.example.sehatqapp.repository.Repository
import org.koin.dsl.module

val repositoryModule = module {
    single { appDatabase(get()) }
    single { Repository(get(), get()) }
}

private fun appDatabase(context: Context) = AppDatabase.getInstance(context).parametersDao()