package com.example.sehatqapp.module

import android.content.Context
import com.example.sehatqapp.network.ApiService
import com.example.sehatqapp.utilities.AppConfig
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.BuildConfig
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { okHttp(get()) }

    single { connect(get()) }

}

private fun okHttp(context: Context): OkHttpClient {
    val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(OkHttpProfilerInterceptor())
        .readTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)

    // chuck setup
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    okHttpClient.addInterceptor(
        httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }
    )
    okHttpClient.addNetworkInterceptor(StethoInterceptor())
    okHttpClient.addInterceptor(ChuckInterceptor(context))

    return okHttpClient.build()
}

// connection setup
private fun connect(context: Context): ApiService {
    val retrofit = Retrofit.Builder()
        .baseUrl(AppConfig.URL)
        .client(okHttp(context))
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    return retrofit.create(ApiService::class.java)
}