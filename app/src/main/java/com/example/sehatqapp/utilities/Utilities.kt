package com.example.sehatqapp.utilities

import java.text.SimpleDateFormat
import java.util.*

object Utilities {

    // retrieve current time
    fun retrieveCurrentTime(): String {
        val sdf = SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH)
        val currentDate = sdf.format(Date())

        return currentDate
    }
}