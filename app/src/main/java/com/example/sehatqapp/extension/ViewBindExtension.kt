package com.example.sehatqapp.extension

import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.example.sehatqapp.R
import com.example.sehatqapp.extension.ViewExtension.loadImage
import com.example.sehatqapp.extension.ViewExtension.onTextChanged
import com.example.sehatqapp.view.search.SearchState
import com.example.sehatqapp.view.search.SearchViewModel

object ViewBindExtension {
    @BindingAdapter("app:loadImage")
    @JvmStatic
    fun setImage(view: ImageView, url: String) {
        view.loadImage(url)
    }

    @BindingAdapter("app:loadFavouriteImage")
    @JvmStatic
    fun setFavouriteImage(view: ImageView, loved: Int) {
        if (loved == 0) {
            view.loadImage(R.drawable.ic_favorite_black)
        } else {
            view.loadImage(R.drawable.ic_favorite_red)
        }
    }

    @BindingAdapter("app:loadArrowImage")
    @JvmStatic
    fun setArrowImage(view: ImageView, arrow: Int) {
        if (arrow == 0) {
            view.loadImage(R.drawable.ic_backarrow_black)
        } else {
            view.loadImage(R.drawable.ic_backarrow)
        }
    }

    @BindingAdapter("app:loadSharedImage")
    @JvmStatic
    fun setSharedImage(view: ImageView, share: Int) {
        if (share == 0) {
            view.loadImage(R.drawable.ic_share_black)
        } else {
            view.loadImage(R.drawable.ic_share_white)
        }
    }

    @BindingAdapter("app:addTextChangedListener")
    @JvmStatic
    fun addTextChangedListener(editText: EditText, viewModel: SearchViewModel?) {
        editText.onTextChanged { editChar -> viewModel?.setState(SearchState.FilterList(editChar)) }
    }
}