package com.example.sehatqapp.extension

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.text.Editable
import android.text.TextWatcher
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.sehatqapp.R

object ViewExtension {
    // check internet connection
    fun Context.isInternetConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val nw = connectivityManager.activeNetwork ?: return false
        val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
        return when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                true
            }
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                true
            }
            else -> false
        }
    }

    fun ImageView.loadImage(url: String) {
        Glide.with(this)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade(800))
            .into(this)
    }

    fun ImageView.loadImage(@DrawableRes resource: Int) {
        Glide.with(this)
            .load(resource)
            .transition(DrawableTransitionOptions.withCrossFade(800))
            .into(this)
    }

    fun EditText.onTextChanged(listener: (p0: CharSequence?) -> Unit) {

        this.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                listener.invoke(p0)
            }

            override fun afterTextChanged(p0: Editable?) {

            }

        })
    }


    // re enable user action after loading bar disappear
    fun Window.enableScreenAction() {
        clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    // user can not do any action while loading bar appear
    fun Window.disableScreenAction() {
        setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }
}