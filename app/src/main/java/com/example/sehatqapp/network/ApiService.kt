package com.example.sehatqapp.network

import com.example.sehatqapp.model.ResponseItemz
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {
    // all category
    @GET("home")
    fun loadProduct(): Single<List<ResponseItemz?>?>
}