package com.example.sehatqapp.base

import androidx.lifecycle.ViewModel
import com.example.sehatqapp.base.BaseState

abstract class BaseViewModel : ViewModel() {
    abstract fun provideState() : BaseState
}