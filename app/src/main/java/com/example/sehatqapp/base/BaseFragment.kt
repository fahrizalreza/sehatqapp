package com.example.sehatqapp.base

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.example.sehatqapp.BR
import com.example.sehatqapp.R
import com.example.sehatqapp.model.ProductItem
import com.example.sehatqapp.view.detailproduct.DetailProductActivity
import com.example.sehatqapp.view.search.SearchActivity

abstract class BaseFragment<binding : ViewDataBinding, state : BaseState, viewModel : BaseViewModel>(
    @LayoutRes val layoutId: Int
) : Fragment() {

    protected lateinit var binding: binding

    protected abstract val viewModel: viewModel

    @Suppress("UNCHECKED_CAST")
    protected val state: state by lazy {
        viewModel.provideState() as state
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        DataBindingUtil.inflate<binding>(inflater, layoutId, container, false).apply {
            binding = this
            lifecycleOwner = this@BaseFragment
            setVariable(BR.viewModel, viewModel)
        }

        initializeView()
        observeViewState(state)
        return binding.root
//        return super.onCreateView(inflater, container, savedInstanceState)
    }

    abstract fun observeViewState(state: state)
    open fun initializeView() {}

    // open detail product activity
    fun openDetailProduct(item: ProductItem?) {
        val intent = Intent(context?.applicationContext, DetailProductActivity::class.java)
        intent.putExtra(KEY_PRODUCT_ID, item?.id)
        intent.putExtra(KEY_PRODUCT_TITLE, item?.title)
        intent.putExtra(KEY_PRODUCT_LOVED, item?.loved)
        intent.putExtra(KEY_PRODUCT_PICTURE_URL, item?.imageUrl)
        intent.putExtra(KEY_PRODUCT_PRICE, item?.price)
        intent.putExtra(KEY_PRODUCT_DESCRIPTION, item?.description)

        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    // open search product activity
    fun openSearchProduct() {
        val intent = Intent(context?.applicationContext, SearchActivity::class.java)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    // constant value
    companion object {
        const val KEY_PRODUCT_ID: String = "productId"
        const val KEY_PRODUCT_TITLE: String = "productTitle"
        const val KEY_PRODUCT_PICTURE_URL: String = "productURL"
        const val KEY_PRODUCT_DESCRIPTION: String = "productDescription"
        const val KEY_PRODUCT_PRICE: String = "productPrice"
        const val KEY_PRODUCT_LOVED: String = "productLoved"
    }
}