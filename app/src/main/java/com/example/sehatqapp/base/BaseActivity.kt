package com.example.sehatqapp.base

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.sehatqapp.BR
import com.example.sehatqapp.R
import com.example.sehatqapp.extension.ViewExtension.isInternetConnected
import com.example.sehatqapp.model.ProductItem
import com.example.sehatqapp.view.detailproduct.DetailProductActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext


abstract class BaseActivity<binding : ViewDataBinding, state : BaseState, viewModel : BaseViewModel>(
    @LayoutRes val layoutId: Int
) : AppCompatActivity() {

    protected lateinit var binding: binding

    protected abstract val viewModel: viewModel

    @Suppress("UNCHECKED_CAST")
    protected val state: state by lazy {
        viewModel.provideState() as state
    }

    private var job = Job()
    val coroutineScope = CoroutineScope(Dispatchers.Main + job)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)
            val controller = window.insetsController
            if (controller != null) {
                controller.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
                controller.systemBarsBehavior =
                    WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        } else {
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN)
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        initializedDataBind()
        initializeView()
        observeViewState(state)
    }

    open fun initializeView() {}

    private fun initializedDataBind() {
        DataBindingUtil.setContentView<binding>(this, layoutId).apply {
            binding = this
            lifecycleOwner = this@BaseActivity
            setVariable(BR.viewModel, viewModel)
//            setVariable(BR.state, state)
            executePendingBindings()
        }
    }

    abstract fun observeViewState(state: state)

    // perform when user press back button
    override fun onBackPressed() {
        if (this.localClassName == "view.main.MainActivity") {
            openDialog()
        }else {
            closePage()
        }
    }

    // check internet connection
    suspend fun isInternetConnected(): Boolean =
        withContext(Dispatchers.Default) {
            applicationContext.isInternetConnected()
        }

    // confirmation box
    private fun openDialog() {
        val builder1 = AlertDialog.Builder(this)
        builder1.setMessage(resources.getString(R.string.exit_app_ind))
        builder1.setCancelable(true)
        builder1.setPositiveButton(
            resources.getString(R.string.yes_ind)
        ) { dialog, id -> finish() }
        builder1.setNegativeButton(
            resources.getString(R.string.no_ind)
        ) { dialog, id -> dialog.cancel() }
        val alert = builder1.create()
        alert.setOnShowListener {
            alert.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.black, null))
            alert.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.black, null))
        }
        alert.show()
    }

    // open detail product activity
    fun openDetailProduct(item: ProductItem?) {
        val intent = Intent(this, DetailProductActivity::class.java)
        intent.putExtra(KEY_PRODUCT_ID, item?.id)
        intent.putExtra(KEY_PRODUCT_TITLE, item?.title)
        intent.putExtra(KEY_PRODUCT_LOVED, item?.loved)
        intent.putExtra(KEY_PRODUCT_PICTURE_URL, item?.imageUrl)
        intent.putExtra(KEY_PRODUCT_PRICE, item?.price)
        intent.putExtra(KEY_PRODUCT_DESCRIPTION, item?.description)

        startActivity(intent)
    }

    // start activity with slide transition
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    // close with slide animation
    fun closePage() {
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    // pop up message
    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    // constant value
    companion object {
        const val KEY_PRODUCT_ID: String = "productId"
        const val KEY_PRODUCT_TITLE: String = "productTitle"
        const val KEY_PRODUCT_PICTURE_URL: String = "productURL"
        const val KEY_PRODUCT_DESCRIPTION: String = "productDescription"
        const val KEY_PRODUCT_PRICE: String = "productPrice"
        const val KEY_PRODUCT_LOVED: String = "productLoved"
    }
}